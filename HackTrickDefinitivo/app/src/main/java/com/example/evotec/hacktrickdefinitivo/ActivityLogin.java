package com.example.evotec.hacktrickdefinitivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = ((EditText)findViewById(R.id.txtUsiario)).getText().toString();
                String password = ((EditText)findViewById(R.id.txtPass)).getText().toString();
                if (usuario.equals("admin") && password.equals("admin") ){
                    Intent formulario = new Intent(ActivityLogin.this, ActivityModoSuper.class );
                    startActivity(formulario);
                }else {
                    Toast.makeText(getApplicationContext(),"Usuario incorrecto", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    Button btnIngresar;
}
