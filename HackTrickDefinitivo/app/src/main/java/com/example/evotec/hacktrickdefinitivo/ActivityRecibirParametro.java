package com.example.evotec.hacktrickdefinitivo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityRecibirParametro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        pasarE1 = (TextView)findViewById(R.id.edtEquipo1);
        pasarE2 = (TextView)findViewById(R.id.edtEquipo2);
        pasarE3 = (TextView)findViewById(R.id.edtEquipo3);
        pasarE4 = (TextView)findViewById(R.id.edtEquipo4);
        pasarE5 = (TextView)findViewById(R.id.edtEquipo5);
        pasarE6 = (TextView)findViewById(R.id.edtEquipo6);
        pasarNombre = (TextView)findViewById(R.id.edtnombreEquipo);
        volverI = (Button)findViewById(R.id.volverI);

        Bundle bundle = this.getIntent().getExtras();
        pasarNombre.setText(bundle.getString("dato7"));
        pasarE1.setText(bundle.getString("dato1"));
        pasarE2.setText(bundle.getString("dato2"));
        pasarE3.setText(bundle.getString("dato3"));
        pasarE4.setText(bundle.getString("dato4"));
        pasarE5.setText(bundle.getString("dato5"));
        pasarE6.setText(bundle.getString("dato6"));

        volverI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (ActivityRecibirParametro.this, ActivityModoSuper.class);
                startActivity(intent);

            }
        });




    }

    TextView pasarE1, pasarE2, pasarE3, pasarE4, pasarE5, pasarE6, pasarNombre;
    Button volverI;
}
