package com.example.evotec.practica8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ActividadMemoriaInterna extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_interna);
        cajaCedula = (TextView)findViewById(R.id.txtCedulaMi);
        cajaNombre = (TextView)findViewById(R.id.txtNombresMi);
        cajaApellidos = (TextView)findViewById(R.id.txtApellidosMi);
        cajaDatos = (TextView)findViewById(R.id.txtDatosMi);

        botonLeer = (Button)findViewById(R.id.btnLeerMi);
        botonEscribir = (Button)findViewById(R.id.btnEscribirMi);

        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
    }
    TextView cajaCedula, cajaNombre, cajaApellidos , cajaDatos;
    Button botonLeer, botonEscribir;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEscribirMi:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cajaCedula.getText().toString()+" , " +cajaApellidos.getText().toString()+" , "+cajaNombre.getText().toString());
                    escritor.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception ex) {
                    log.e("Archivo MI","Error ")
                    e.printStackTrace();
                }
        }

    }
}
