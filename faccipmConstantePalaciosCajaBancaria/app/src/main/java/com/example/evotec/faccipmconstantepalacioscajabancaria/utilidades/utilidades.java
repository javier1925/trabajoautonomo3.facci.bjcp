package com.example.evotec.faccipmconstantepalacioscajabancaria.utilidades;

public class utilidades {
    //definimos las constantes
    public static final String TABLA_USUARIO = "usuario";
    public static final String CAMPO_IDCAJA = "id_caja";
    public static final String CAMPO_UBICACIONCAJA = "ubicacion_caja";
    public static final String CAMPO_CEDULACAJA = "cedula_caja";
    public static final String CAMPO_SALDOINICIAL = "saldo_inicial";


    public static final String CREAR_TABLA_USUARIO =
            "CREATE TABLE "+TABLA_USUARIO+" ("+CAMPO_IDCAJA+" INTEGER, "+CAMPO_UBICACIONCAJA+" TEXT, "+CAMPO_CEDULACAJA+" TEXT, "+CAMPO_SALDOINICIAL+" TEXT)";

}
