package com.example.evotec.faccipmconstantepalacioscajabancaria.entidades;

public class usuario {
    private Integer id_caja;
    private String ubicacion_caja;
    private String cedula_caja;
    private String saldo_inicial;

    public usuario(Integer id_caja, String ubicacion_caja, String cedula_caja, String saldo_inicial) {
        this.id_caja = id_caja;
        this.ubicacion_caja = ubicacion_caja;
        this.cedula_caja = cedula_caja;
        this.saldo_inicial = saldo_inicial;
    }

    public Integer getId_caja() {
        return id_caja;
    }

    public void setId_caja(Integer id_caja) {
        this.id_caja = id_caja;
    }

    public String getUbicacion_caja() {
        return ubicacion_caja;
    }

    public void setUbicacion_caja(String ubicacion_caja) {
        this.ubicacion_caja = ubicacion_caja;
    }

    public String getCedula_caja() {
        return cedula_caja;
    }

    public void setCedula_caja(String cedula_caja) {
        this.cedula_caja = cedula_caja;
    }

    public String getSaldo_inicial() {
        return saldo_inicial;
    }

    public void setSaldo_inicial(String saldo_inicial) {
        this.saldo_inicial = saldo_inicial;
    }
}
