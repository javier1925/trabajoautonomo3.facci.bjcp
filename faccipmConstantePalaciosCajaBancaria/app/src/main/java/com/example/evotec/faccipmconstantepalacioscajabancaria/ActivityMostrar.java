package com.example.evotec.faccipmconstantepalacioscajabancaria;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.evotec.faccipmconstantepalacioscajabancaria.utilidades.utilidades;

public class ActivityMostrar extends AppCompatActivity {
    ConexionSQLiteHelper con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar);
    }
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnConsultar:
                consultar();
          //      consultarSql();
                break;


    }
    private void consultar() {
        SQLiteDatabase db=con.getReadableDatabase();
        String[] parametros={campoId.getText().toString()};
        String[] campos={Utilidades.CAMPO_NOMBRE,Utilidades.CAMPO_TELEFONO};

        try {
            Cursor cursor =db.query(utilidades.TABLA_USUARIO,campos,utilidades.CAMPO_IDCAJA+"=?",parametros,null,null,null);
            cursor.moveToFirst();
            CAMPO_UBICACIONCAJA.setText(cursor.getString(0));
            CAMPO_CEDULACAJA.setText(cursor.getString(1));
            CAMPO_SALDOINICIAL.setText(cursor.getString(1));

            cursor.close();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"El documento no existe",Toast.LENGTH_LONG).show();
            limpiar();
        }


    }
}
