package com.example.evotec.consultaprueba;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Consultar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar);

        edit1 = (EditText) findViewById(R.id.edit1);
        edit2 = (EditText) findViewById(R.id.edit2);
        edit3 = (EditText) findViewById(R.id.edit3);
        edit4 = (EditText) findViewById(R.id.edit4);



            }
    EditText edit1, edit2, edit3, edit4;

    public void consulta(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(Consultar.this,

                "administracion", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String dni = edit1.getText().toString();

        Cursor fila = bd.rawQuery(

                "select nombre, fecha, materia from usuario where dni=" + dni, null);

        if (fila.moveToFirst()) {

            edit2.setText(fila.getString(1));
            edit3.setText(fila.getString(2));
            edit4.setText(fila.getString(3));

        } else

            Toast.makeText(Consultar.this, "No existe ningún usuario con ese dni",

                    Toast.LENGTH_SHORT).show();

        bd.close();

    }

}
