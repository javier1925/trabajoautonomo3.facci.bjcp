package com.example.evotec.pruebafinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnRegistro = (Button)findViewById(R.id.btnRegistro);


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ActivityRegistro.class);
                startActivity(intent);
            }
        });

        conexionSQLiteHelper con = new conexionSQLiteHelper(this,"bd_usuarios", null,1);
    }
    Button btnRegistro, btnConsulta;
}
