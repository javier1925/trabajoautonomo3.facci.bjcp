package com.example.evotec.pruebafinal.entidades;

public class Usuario {
    private Integer id;
    private String nombre;
    private String materia;
    private String fecha;

    public Usuario(Integer id, String nombre, String materia, String fecha) {
        this.id = id;
        this.nombre = nombre;
        this.materia = materia;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
